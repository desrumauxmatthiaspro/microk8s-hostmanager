FROM maven:3.6.1-jdk-8-alpine


COPY . /project
RUN cd /project && mvn clean compile assembly:single

FROM openjdk:8-alpine3.8

RUN mkdir -p "/usr/local/kubernetes_hostmanager"
COPY --from=0 /project/target/kubernetes_hostmanager-1.0.0-jar-with-dependencies.jar /usr/local/kubernetes_hostmanager/kubernetes_hostmanager-1.0.0.jar

CMD ["java", "-jar", "/usr/local/kubernetes_hostmanager/kubernetes_hostmanager-1.0.0.jar"]