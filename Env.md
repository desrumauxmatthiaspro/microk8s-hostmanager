# Environment

| Name | Description | Default |
| :- | :- | :-: |
| ETC_HOST_FILE_PATH | It's the path to the host file | `/etc/hosts` |
| ANCHOR_START_PATTERN | It's the anchor who start the kubernetes hosts managed | `###> %s ###` |
| ANCHOR_END_PATTERN | It's the anchor who end the kubernetes hosts managed | `###< %s ###` |
| OVERWRITE_EXISTING_HOSTS | It's allow the manager to overwrite already defined managed hosts | `true` |
| INGRESS_REFRESH_DELAY | It's the ingresses watches refresh delay in millisecond | `5000` |
| USE_SPECIFIC_IP_ADDR | Enable the use of a specific ip addr | `false` |
| SPECIFIC_IP_ADDR | Use a specific ip addr to update the `/etc/hosts` | `127.0.0.1` | 
