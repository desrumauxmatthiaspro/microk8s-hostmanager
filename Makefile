dev-install:
	@rm -rf helm/microk8s-hostmanager-0.1.0.tgz
	@cd helm && microk8s.helm3 package . --set hosts_file_path=$(PWD)/mock/hosts --set image=localhost:32000/microk8s-hostmanager
	@microk8s.helm3 upgrade microk8s-hostmanager helm/microk8s-hostmanager-0.1.0.tgz --install

install:
	@rm -rf helm/microk8s-hostmanager-0.1.0.tgz
	@cd helm && microk8s.helm3 package .
	@microk8s.helm3 upgrade microk8s-hostmanager helm/microk8s-hostmanager-0.1.0.tgz --install


clean:
	@microk8s.kubectl delete secrets sh.helm.release.v1.microk8s-hostmanager.v1 || true
	@microk8s.kubectl delete deployments microk8s-hostmanager -n kube-system || true
	@microk8s.kubectl delete ingress kube-system-ingress-grafana -n kube-system || true
	@microk8s.kubectl delete ingress kube-system-ingress-https -n kube-system || true
	@microk8s.kubectl delete secrets kube-system-ingress-tls -n kube-system || true

log:
	@microk8s.kubectl logs -f $(shell microk8s.kubectl get pods -A -o jsonpath='{$$.items[?(@.metadata.labels.app=="microk8s-hostmanager")].metadata.name}' || exit 1) -n kube-system

build:
	@docker build -t localhost:32000/microk8s-hostmanager:latest .
	@docker push localhost:32000/microk8s-hostmanager:latest
	@microk8s.kubectl delete pod $(shell microk8s.kubectl get pods -A -o jsonpath='{$$.items[?(@.metadata.labels.app=="microk8s-hostmanager")].metadata.name}' || exit 1) -n kube-system --force --grace-period=0 || true