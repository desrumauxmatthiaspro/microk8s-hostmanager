# Microk8s Hostmanager

This small utility will update your `/etc/hosts` file.
The main support will be for the microk8s application.
This application can be compatible with any kubernetes solution.


## Install

You must install a kubernetes solution.
The supported kubernetes solution for it's `microk8s`.

```bash
sudo snap install microk8s --classic
```

Then you can use this helm chart with the following script.

```bash
microk8s.helm3 upgrade microk8s-hostmanager https://gitlab.com/desrumauxmatthiaspro/microk8s-hostmanager/-/jobs/artifacts/master/raw/microk8s-hostmanager-0.1.0.tgz?job=helm --install
``` 

If you lookup into your `/etc/hosts` file you can see the ingress hosts defined in it.

## Kubernetes predefined hosts

If you have enable one or more microk8s extension(s), you will find here the hosts you can used to access it.
You don't have to do a `microk8s.kubectl proxy` to reach them.

| Host | Service | Microk8s Extensions |
| :- | :- | :-: |
| Kubernetes Dashboard | https://dashboard.microk8s/ | `dashboard` |
| Grafana Dasboard (Login=`admin:admin`) | https://grafana.microk8s/api/v1/namespaces/kube-system/services/monitoring-grafana/proxy | `dashboard` |