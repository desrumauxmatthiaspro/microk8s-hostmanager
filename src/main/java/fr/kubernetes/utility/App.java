package fr.kubernetes.utility;

import fr.kubernetes.utility.handlers.HostsFileHandler;
import fr.kubernetes.utility.handlers.KubernetesHandler;
import lombok.Cleanup;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;

import java.io.PrintWriter;

public class App 
{
    /**
     * It's the app logger
     */
    public static final Logger LOGGER = Logger.getLogger(App.class);

    /**
     * Entrypoint of the application
     *
     * @param args
     */
    public static void main( String[] args )
    {
        BasicConfigurator.configure();
        LOGGER.info("The kubernetes hostmanager is starting.");
        try {
            Env.initialize();
            HostsFileHandler hostsFileHandler = new HostsFileHandler();
            hostsFileHandler.loadHosts();

            @Cleanup
            KubernetesHandler kubernetesHandler = new KubernetesHandler(hostsFileHandler);
            kubernetesHandler.watch();
            kubernetesHandler.join();
        } catch (Exception e) {
            LOGGER.fatal("Unhandled exception", e);
            System.exit(1);
        }
    }
}
