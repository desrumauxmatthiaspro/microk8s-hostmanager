package fr.kubernetes.utility;

import io.vavr.Function2;
import lombok.Getter;

import java.util.Map;
import java.util.function.Function;

@Getter
public class Env {

    /**
     * It's the etc host file path
     */
    String etcHostFilePath;

    /**
     * It's the start anchor inside the hosts file
     */
    String anchorStartPattern;

    /**
     * It's allow to overwrite existing hosts
     */
    boolean overwriteExistingHosts;

    /**
     * It's the end anchor inside the hosts file
     */
    String anchorEndPattern;

    /**
     * It's the kubeconfig file path
     */
    String kubeconfigFilePath;

    /**
     * It's the delay between each ingress request
     */
    int ingressesRefreshDelay;

    /**
     * Use a specific ip addr for the backend
     */
    boolean useSpecificIpAddr;

    /**
     * It's the specific namespace to watch
     */
    String speificIpAddr;

    /**
     * It's the instance of the env variable
     */
    private static Env INSTANCE = null;

    /**
     * It's the ID used inside the anchor
     */
    private static final String ID = "dm.kubemanager";

    /**
     * @return The environment
     */
    public static Env getInstance()
    {
        return INSTANCE;
    }

    /**
     * Initialize the environment
     */
    public static void initialize()
    {
        if (INSTANCE != null) {
            return;
        }

        App.LOGGER.info("Loading environment variables");

        INSTANCE = new Env();
        Map<String, String> env = System.getenv();

        Function2<String, Boolean, Boolean> getBooleanEnv = (key, fallback) -> {
            if (!env.containsKey(key)) {
                return fallback;
            }

            String lowered = env.get(key).toLowerCase();
            if (lowered.equals("true") || lowered.equals("yes")) {
                return true;
            }

            return false;
        };

        Function2<String, Integer, Integer> getIntegerEnv = (key, fallback) -> {
            if (!env.containsKey(key)) {
                return fallback;
            }

            return Integer.valueOf(env.get(key));
        };

        INSTANCE.etcHostFilePath = env.getOrDefault("ETC_HOST_FILE_PATH", "/etc/hosts");
        INSTANCE.anchorStartPattern = env.getOrDefault("ANCHOR_START_PATTERN", "###> %s ###");
        INSTANCE.anchorStartPattern = String.format(INSTANCE.anchorStartPattern, ID);
        INSTANCE.anchorEndPattern = env.getOrDefault("ANCHOR_END_PATTERN", "###< %s ###");
        INSTANCE.anchorEndPattern = String.format(INSTANCE.anchorEndPattern, ID);
        INSTANCE.overwriteExistingHosts = getBooleanEnv.apply("OVERWRITE_EXISTING_HOSTS", true);
        INSTANCE.kubeconfigFilePath = env.getOrDefault("KUBECONFIG_FILE_PATH", String.format(
            "%s/.kube/config",
            env.get("HOME")
        ));
        INSTANCE.ingressesRefreshDelay = getIntegerEnv.apply("INGRESS_REFRESH_DELAY", 5000);
        INSTANCE.useSpecificIpAddr = getBooleanEnv.apply("USE_SPECIFIC_IP_ADDR", false);
        INSTANCE.speificIpAddr = env.getOrDefault("SPECIFIC_IP_ADDR", "127.0.0.1");

        App.LOGGER.info("Environment variables are loaded");
    }

}
