package fr.kubernetes.utility.handlers;

import fr.kubernetes.utility.App;
import fr.kubernetes.utility.Env;
import lombok.Cleanup;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HostsFileHandler {

    /**
     * It's the other hosts lines
     */
    private List<String> otherHostsLines = new ArrayList<>();

    /**
     * It's the hosts managed
     */
    private Map<String, List<String>> hosts = new HashMap<>();

    /**
     * Load the host file
     */
    public synchronized void loadHosts() throws IOException, OutOfMemoryError
    {
        File file = new File(Env.getInstance().getEtcHostFilePath());
        App.LOGGER.info(String.format("'%s' hosts file is going to be loaded.", file.getAbsolutePath()));
        if (!file.exists() || !file.canRead()) {
            throw new IOException(String.format("'%s' hosts file doesn't exists or can't be read.", file.getAbsolutePath()));
        }

        @Cleanup
        BufferedReader stream = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

        otherHostsLines.clear();
        hosts.clear();

        String startPattern = Env.getInstance().getAnchorStartPattern();
        String endPattern = Env.getInstance().getAnchorEndPattern();

        Pattern pattern = Pattern.compile("[^ \t]+");
        boolean hostManaged = false;
        while (stream.ready()) {
            String line = stream.readLine();
            line = line.trim();
            if (line.equals(startPattern)) {
                hostManaged = true;
                continue;
            } else if (line.equals(endPattern)) {
                hostManaged = false;
                continue;
            }

            if (hostManaged) {
                if (line.startsWith("#")) {
                    continue;
                }

                Matcher matcher = pattern.matcher(line);
                if (matcher.find()) {
                    String host = matcher.group(0);
                    if (!hosts.containsKey(host)) {
                        hosts.put(host, new ArrayList<>());
                    }

                    List<String> urls = hosts.get(host);
                    while (matcher.find()) {
                        urls.add(matcher.group(0));
                    }
                }
            } else {
                otherHostsLines.add(line);
            }
        }
    }

    /**
     * Load the host file
     */
    public synchronized void saveHosts() throws IOException, OutOfMemoryError
    {
        File file = new File(Env.getInstance().getEtcHostFilePath());
        App.LOGGER.info(String.format("'%s' hosts file is going to be loaded.", file.getAbsolutePath()));
        if (!file.exists() || !file.canWrite()) {
            throw new IOException(String.format("'%s' hosts file doesn't exists or can't be writed.", file.getAbsolutePath()));
        }

        @Cleanup
        BufferedWriter stream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));

        String startPattern = Env.getInstance().getAnchorStartPattern();
        String endPattern = Env.getInstance().getAnchorEndPattern();

        for (String line : otherHostsLines) {
            if (line.trim().isEmpty()) {
                continue;
            }
            stream.write(line + "\n");
        }

        stream.write(startPattern + "\n");
        for (String host : hosts.keySet()) {
            List<String> list = hosts.get(host);
            if (list.size() == 0) {
                continue;
            }

            String line = host;
            for (String backend : list) {
                line += "\t" + backend;
            }

            stream.write(line + "\n");
        }
        stream.write( endPattern + "\n");
    }

    /**
     * Add a host to the hosts file
     *
     * @param host
     * @param backends
     * @param dropIfExisting
     */
    private synchronized void putHosts(String host, List<String> backends, boolean dropIfExisting)
    {
        if (dropIfExisting) {
            for (String key : hosts.keySet()) {
                List<String> alreadyBackends = hosts.get(key);
                List<Integer> indexes = new ArrayList<>();
                for (String backend : backends) {
                    int i = 0;
                    for (String alreadyBackend : alreadyBackends) {
                        if (alreadyBackend.equals(backend) && !indexes.contains(i)) {
                            indexes.add(i);
                        }
                        i++;
                    }
                }

                Collections.sort(indexes, Comparator.reverseOrder());
                for (Integer index : indexes) {
                    alreadyBackends.remove(index.intValue());
                }
            }
        }

        if (!hosts.containsKey(host)) {
            hosts.put(host, backends);
        } else {
            List<String> push = new ArrayList<>();
            for (String backend : backends) {
                boolean newBackend = true;
                for (String alreadyBackend : hosts.get(host)) {
                    if (alreadyBackend.equals(backend)) {
                        newBackend = false;
                    }
                }
                if (newBackend) {
                    push.add(backend);
                }
            }
            hosts.get(host).addAll(push);
        }
    }

    /**
     * Add a host to the lists
     *
     * @param host
     * @param backend
     */
    public synchronized void putHost(String host, String backend)
    {
        List<String> backends = new ArrayList<>();
        backends.add(backend);
        putHosts(host, backends, Env.getInstance().isOverwriteExistingHosts());
    }

    /**
     * Add the hosts to the lists
     *
     * @param host
     * @param backends
     */
    public synchronized void putHost(String host, List<String> backends)
    {
        putHosts(host, backends, Env.getInstance().isOverwriteExistingHosts());
    }
}
