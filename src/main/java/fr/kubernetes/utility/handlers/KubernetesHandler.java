package fr.kubernetes.utility.handlers;

import fr.kubernetes.utility.App;
import fr.kubernetes.utility.Env;
import io.kubernetes.client.ApiClient;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.apis.NetworkingApi;
import io.kubernetes.client.apis.ExtensionsV1beta1Api;
import io.kubernetes.client.models.V1LoadBalancerIngress;
import io.kubernetes.client.models.V1beta1Ingress;
import io.kubernetes.client.models.V1beta1IngressList;
import io.kubernetes.client.models.V1beta1IngressRule;
import io.kubernetes.client.util.Config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class KubernetesHandler {

    /**
     * This handler handler the /etc/hosts file
     */
    private final HostsFileHandler hostsFile;

    /**
     * Create a kubernetes handler
     *
     * @param hostsFile
     *
     * @throws IOException
     */
    public KubernetesHandler(HostsFileHandler hostsFile) throws IOException
    {
        ApiClient client = Config.fromCluster();
        Configuration.setDefaultApiClient(client);
        this.hostsFile = hostsFile;
    }

    /**
     * Continue to watch the ingresses
     */
    private boolean watching = false;

    /**
     * Watch the ingresses
     */
    private void watchIngress()
    {
        try {
            while (watching) {

                ExtensionsV1beta1Api api = new ExtensionsV1beta1Api();
                V1beta1IngressList ingresses = api.listIngressForAllNamespaces(null, null, null, null, null, null, null, null, null);

                for (V1beta1Ingress ingress : ingresses.getItems()) {

                    List<String> backend = new ArrayList<>();
                    for (V1beta1IngressRule rule : ingress.getSpec().getRules()) {
                        backend.add(rule.getHost());
                    }

                    if (Env.getInstance().isUseSpecificIpAddr()) {
                        hostsFile.putHost(Env.getInstance().getSpeificIpAddr(), backend);
                    } else {
                        for (V1LoadBalancerIngress lbIngress : ingress.getStatus().getLoadBalancer().getIngress()) {
                            hostsFile.putHost(lbIngress.getIp(), backend);
                        }
                    }
                }

                hostsFile.saveHosts();
                Thread.sleep(Env.getInstance().getIngressesRefreshDelay());
            }
        } catch (Exception e) {
            App.LOGGER.fatal("Ingresses watch has crash.", e);
            System.exit(1);
        }
    }

    /**
     * It's the watch thread
     */
    private Thread thread = null;

    /**
     * Watch the kubernetes handlers
     */
    public void watch()
    {
        if (thread != null) {
            return;
        }

        watching = true;
        thread = new Thread(this::watchIngress);
        thread.start();
    }

    /**
     * Await the thread of the ingresses watch been closed
     */
    public void join() throws InterruptedException
    {
        if (thread == null) {
            return;
        }

        thread.join();
    }

    /**
     * Stop the watching of the ingresses
     *
     * @throws InterruptedException
     */
    public void close() throws InterruptedException
    {
        if (thread == null) {
            return;
        }

        watching = false;
        thread.join();
    }
}
